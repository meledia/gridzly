Gridzly
===================


Gridzly is a lightweight, CSS-first solution to laying out consistent UIs for the modern web. Our goal is to make planning and implementing layouts on the web *bearable* by providing an adaptive, responsive, and customizable 12-point grid system. We don't want Gridzly to just be *easy* to use, but fun to use, too!

You can learn more about the theory behind Gridzly below, but we thought it'd be easier to put the important stuff first. If you're looking to get right into the source, check out this structure guide and jump in!

**Source Code Structure**

 - Minified, production-ready framework: ``dist/``
 - Unminified source stylesheets: ``src/``
 - Markdown- and HTML-formatted documentation: ``docs/``

----------


CSS-first? Sounds like marketing lingo...
-------------

**TODO:** explain Gridzly with all of the evidence, sources, and imagination you've been collecting!
